package hereBeDragons;

import java.util.Random;
import java.util.Scanner;

public class HereBeDragons
{
	public static boolean victorious = false;
	public static boolean foundKey = false;
		
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

		char[][] dungeon =
		{
			{'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'},
			{'X', 'H', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', 'D', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', ' ', ' ', ' ', ' ', ' ', ' ', 'X', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', 'K', 'X', 'X', ' ', ' ', ' ', ' ', ' ', 'X'},
			{'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'}
		};
		
		Scanner scan = new Scanner(System.in);
		Random rand = new Random();
		int exitX = 0, exitY = 0;
				
		
		//Random exit location, but not near Hero, Dragon and Key (in all directions, including diagonals)
		//The exit door cannot be diagonal to the Dragon, or else the game turns impossible to win!
		switch (rand.nextInt(4))
		{
		case 0: //Upper Wall
			do
			{
				exitX = rand.nextInt(dungeon.length - 2) + 1;
			}
			while (dungeon[1][exitX] != ' ' ||
					dungeon[1][exitX - 1] == 'H' ||
					dungeon[1][exitX + 1] == 'H' ||
					dungeon[1][exitX - 1] == 'D' ||
					dungeon[1][exitX + 1] == 'D' ||
					dungeon[1][exitX - 1] == 'K' ||
					dungeon[1][exitX + 1] == 'K');
			
			dungeon[0][exitX] = 'E';
			break;
		case 1: //Left Wall
			do
			{
				exitY = rand.nextInt(dungeon.length - 2) + 1;
			}
			while (dungeon[exitY][1] != ' ' ||
					dungeon[exitY - 1][1] == 'H' ||
					dungeon[exitY + 1][1] == 'H' ||
					dungeon[exitY - 1][1] == 'D' ||
					dungeon[exitY + 1][1] == 'D' ||
					dungeon[exitY - 1][1] == 'K' ||
					dungeon[exitY + 1][1] == 'K');
			
			dungeon[exitY][0] = 'E';
			break;
		case 2: //Bottom Wall
			do
			{
				exitX = rand.nextInt(dungeon.length - 2) + 1;
			}
			while (dungeon[dungeon.length - 2][exitX] != ' ' ||
					dungeon[dungeon.length - 2][exitX - 1] == 'H' ||
					dungeon[dungeon.length - 2][exitX + 1] == 'H' ||
					dungeon[dungeon.length - 2][exitX - 1] == 'D' ||
					dungeon[dungeon.length - 2][exitX + 1] == 'D' ||
					dungeon[dungeon.length - 2][exitX - 1] == 'K' ||
					dungeon[dungeon.length - 2][exitX + 1] == 'K');
			
			dungeon[dungeon.length - 1][exitX] = 'E';
			break;
		case 3: //Right Wall
			do
			{
				exitY = rand.nextInt(dungeon.length - 2) + 1;
			}
			while (dungeon[exitY][dungeon[0].length - 2] != ' ' ||
					dungeon[exitY - 1][dungeon[0].length - 2] == 'H' ||
					dungeon[exitY + 1][dungeon[0].length - 2] == 'H' ||
					dungeon[exitY - 1][dungeon[0].length - 2] == 'D' ||
					dungeon[exitY + 1][dungeon[0].length - 2] == 'D' ||
					dungeon[exitY - 1][dungeon[0].length - 2] == 'K' ||
					dungeon[exitY + 1][dungeon[0].length - 2] == 'K');
			
			dungeon[exitY][dungeon[0].length - 1] = 'E';
			break;
		}
		
		dungeonLoop:
		while (!victorious)
		{
			printDungeon(dungeon);
			
			if(dragonBreath(dungeon))
			{
				System.out.println("And thus the hero has failed in his quest. Alas, he has been slain by the Dragon's deadly breath... Game Over");
				break dungeonLoop;
			}
			else
			{
				if (foundKey)
				{
					System.out.print("The hero has found the key! Now he must find the exit, but beware of the Dragon in the dungeon! Enter your next move: ");
				}
				else
				{
					System.out.print("You are the hero in his quest! Find the key to exit the dungeon while avoiding the terrifying Dragon! Enter your next move: ");
				}
			}
				
			char whereToGo = scan.next().charAt(0);
		
			switch (whereToGo)
			{
			case 'w':
				move(dungeon, 'u');	//up
				break;
			case 'a':
				move(dungeon, 'l');	//left
				break;
			case 's':
				move(dungeon, 'd');	//down
				break;
			case 'd':
				move(dungeon, 'r');	//right
				break;
			case 'x':
				System.out.println("This time, the hero's quest will be left untold...");
				break dungeonLoop;
			}
		}
		
		if(victorious)
		{
			printDungeon(dungeon);
			System.out.println("By bravely fleeing the Dragon, the Hero emerges victorious yet again!");
		}
	
		scan.close();
	}
	
	public static void printDungeon (char[][] dungeon)
	{
		for (int line = 0; line < dungeon.length; line++)
		{
			for (int column = 0; column < dungeon[line].length; column++)
			{
				System.out.print(dungeon[line][column]);
			}
			System.out.println();
		}
		
		System.out.println();
	}
	
	public static void move (char[][] dungeon, char direction)
	{
		// We should pass the hero, key and dragon positions by argument. But since we are not using classes
		// yet, that would be messy (considering x and y coordinate for each, etc.).
		//Instead, this routine starts by identifying the hero's position in the dungeon
		
		int heroCurrentPosX = 0;
		int heroCurrentPosY = 0;
		
		findHero:
		for (int line = 0; line < dungeon.length; line++)
		{
			for (int column = 0; column < dungeon.length; column++)
			{
				if (dungeon[line][column] == 'H')
				{
					heroCurrentPosX = column;
					heroCurrentPosY = line;
					break findHero;
				}
			}
		}
		
		int heroNewPosX = heroCurrentPosX;
		int heroNewPosY = heroCurrentPosY;
		
		switch(direction)
		{
		case 'u':
			heroNewPosY--;
			break;
		case 'l':
			heroNewPosX--;
			break;
		case 'd':
			heroNewPosY++;
			break;
		case 'r':
			heroNewPosX++;
			break;
		}
		
		if (dungeon[heroNewPosY][heroNewPosX] == ' ' || dungeon[heroNewPosY][heroNewPosX] == 'K')
		{
			if (dungeon[heroNewPosY][heroNewPosX] == 'K')
				foundKey = true;
			
			dungeon[heroCurrentPosY][heroCurrentPosX] = ' ';
			dungeon[heroNewPosY][heroNewPosX] = 'H';
		}
		else if (dungeon[heroNewPosY][heroNewPosX] == 'E')
		{
			if (foundKey)
			{
				dungeon[heroCurrentPosY][heroCurrentPosX] = ' ';
				dungeon[heroNewPosY][heroNewPosX] = 'H';
				victorious = true;
			}
			else
				System.out.println("The exit door is locked.");
		}
	}
	
	public static boolean dragonBreath (char dungeon[][])
	{
		int heroPosX = 0;
		int heroPosY = 0;
		
		findHero:
		for (int line = 0; line < dungeon.length; line++)
		{
			for (int column = 0; column < dungeon.length; column++)
			{
				if (dungeon[line][column] == 'H')
				{
					heroPosX = column;
					heroPosY = line;
					break findHero;
				}
			}
		}
		
		if (dungeon[heroPosY - 1][heroPosX] == 'D' || dungeon[heroPosY + 1][heroPosX] == 'D')
		{
			return true;
		}
		else
			return false;
	}	
}
